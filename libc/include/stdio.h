#ifndef _STDIO_H
#define _STDIO_H 1
 
#include <sys/cdefs.h>
 
#ifdef __cplusplus
extern "C" {
#endif
 

int putchar(int);
int printf(const char* restrict format, ...);
int sprintf(char *out, const char *format, ...);
int puts(const char* string);
char *gets(char *str);
#ifdef __cplusplus
}
#endif
 
#endif
