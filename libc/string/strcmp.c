#include <string.h>
int strcmp ( const char * str1, const char * str2 )
{
	if(strlen(str1)>strlen(str2))
		return 1;
	if(strlen(str1)<strlen(str2))
		return -1;
	return memcmp(str1, str2, strlen(str1));
}
