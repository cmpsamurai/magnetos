#!/bin/sh
set -e
. ./build.sh
 
mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub
 
cp sysroot/boot/magnetos.kernel isodir/boot/magnetos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "MagnetOs" {
	multiboot /boot/magnetos.kernel
}
EOF
grub-mkrescue -o magnetos.iso isodir
