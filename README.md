### MagentOs ###

This project aims at creating an as functional as possible Operating System.
This is my second operating system as I have already wrote a simple one in college before. But this one I am trying to write it professionally.
I am more concerned with design / extendibility and scalability here.

* Project Started 17/11/2014
* v0.1
* The project was based on the [Meaty Skeleton] from OSDev wiki (http://wiki.osdev.org/Meaty_Skeleton)

### How do I get set up? ###

* You have to setup a set up a GCC Cross-Compiler for i686-elf
* Install qemu for virtualization
* the project contains a clean / iso / qemu scripts.
* To run project just call the qemu.sh script.


### Contribution guidelines ###

* Design
* Code review
* Code contribution

### Who do I talk to? ###

* CMPSamurai (Moustafa Mahmoud) moustafa@cmpsamurai.com
* Mahmoud Aladdin	mahmoud.aladdin@gmail.com
