#ifndef _KERNEL_MEMORY_MANAGER
#define _KERNEL_MEMORY_MANAGER
#include <kernel/multiboot.h>

/*
	Takes the information passed from GRUB and initializes physical memory information
*/
void init_memory(multiboot_info_t* mbd);

#endif
