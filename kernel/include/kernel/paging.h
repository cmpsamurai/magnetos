#ifndef _KERNEL_PAGING_H
#define _KERNEL_PAGING_H

/*
	Returns physical address of any virtual address
	This function uses the fact that the last pde ( Page Directory) is mapped to the pde itself
*/

void * get_physaddr(void * virtualaddr);

// Mapping page alligned physical page to virtual page.
void map_page(void * physaddr, void * virtualaddr, unsigned int flags);

/*
	Setups Paging for system (responsible of replacing the initial paging schema that was set up during initial booting )
*/
void setup_paging();

#endif

