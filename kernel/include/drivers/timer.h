// timer.h -- Defines the interface for all PIT-related functions.
// Written for JamesM's kernel development tutorials.

#ifndef _KERNEL_TIMER_H
#define _KERNEL_TIMER_H

#include "common.h"

void init_timer(u32int frequency);

#endif
