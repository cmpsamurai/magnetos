#include <kernel/multiboot.h>
#include <common.h>

void print_memory_map(multiboot_info_t* mbd,memory_map_t* mmap)
{
	puts("Physical Memory Map :\n\n");
	puts("----------------------------------------------------------------\n");
	while(mmap < mbd->mmap_addr + mbd->mmap_length) {
		printf("base address %0x%08x length  %08x%08x  type %d \n",mmap-> base_addr_high ,mmap-> base_addr_low ,mmap-> length_high,mmap-> length_low, mmap->type);
		mmap = (memory_map_t*) ( (unsigned int)mmap + mmap->size + sizeof(unsigned int) );
	}
}

void init_memory(multiboot_info_t* mbd)
{
	memory_map_t* mmap = mbd->mmap_addr;
	print_memory_map(mbd,mmap);
	
}

