#include <string.h>
#include <stdio.h>
 
#include <kernel/tty.h>
#include <kernel/descriptor_tables.h>
#include <drivers/timer.h>
#include <drivers/keyboard.h>
#include <kernel/shell.h>
#include <kernel/memorymanager.h>






void kernel_early(void* mbd, u32int magic)
{	
	terminal_initialize();

	puts("+-+-+-+-+-+-+ +-+-+ +-+ +-+-+-+ \n");
	puts("|M|a|g|n|e|t| |O|s| |v| |0|.|1| \n");
 	puts("+-+-+-+-+-+-+ +-+-+ +-+ +-+-+-+ \n");
 	puts("Now We Rock ;) \n\n");

	init_descriptor_tables();
	init_memory(mbd);
	//setup_paging();
	//init_timer(50); // Initialise timer to 50Hz
	keyboard_install();	
	init_shell();
}
 
void kernel_main()
{
	
	//asm volatile ("int $0x4");
	//asm volatile ("int $0x10");
	int i,j,k;
	
	puts("\n\nTesting virtual to physical translation (pde trick)\n");
	puts("----------------------------------------------------------------\n");
	printf("\n\nvirutal address : %x  physical address : %x\n",&i,get_physaddr(&i));
	printf("virutal address : %x  physical address : %x\n",&j,get_physaddr(&j));
	printf("virutal address : %x  physical address : %x\n",&k,get_physaddr(&k));

	
	shell();
	while(1){}

}
