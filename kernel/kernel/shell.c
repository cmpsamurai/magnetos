/* shell.c */

#include <stdio.h>
#include <string.h>
#include <kernel/shell.h>
void init_shell()
{
  /* shell.c */
 add_new_command("help", "You code this one.", help_command);
 add_new_command("", "", empty_command);
}


command_table_t CommandTable[MAX_COMMANDS];
int NumberOfCommands=0;

void shell()
{
	char input_string[1024];
	void (*command_function)(void);
	while(strcmp(input_string,"exit")!=0)
	{
		puts("\nMy_Prompt>");
		gets(&input_string);


		if(	strcmp(input_string,"exit")==0)
		{
			break;
		}
		
		/* shell.c */
		int i = findCommand(input_string);

		if(i >= 0)
		 {
		   command_function = CommandTable[i].function;
		   command_function();
		 }
		else
		 {
		   printf("Incorrect or unidentified command :'%s'\n",input_string);
		 }
	}
	puts("exiting shell\n");

}	

/* shell.c */
void add_new_command(char *name, char* description, void *function)
{
 if(NumberOfCommands + 1 < MAX_COMMANDS)
   {
           CommandTable[NumberOfCommands].name = name;
           CommandTable[NumberOfCommands].description = description;
           CommandTable[NumberOfCommands].function = function;
		   NumberOfCommands++;
   }
  return;
}


/* shell.c */
int findCommand(char* command)
   {
	
     int i;
     int ret;
 
     for(i = 0; i < NumberOfCommands; i++)
       {
           ret = strcmp(command, CommandTable[i].name);
           if(ret == 0)
             {
               return i;
             }
          
       }
	  return -1;
   }



void empty_command()
   {
   }


void help_command()
   {
	puts("this is help\n");
   }

