#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
 
#include <drivers/vga.h>
#include <drivers/portio.h>
 
size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

void terminal_initialize(void)
{
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
	terminal_buffer = VGA_MEMORY;
	for ( size_t y = 0; y < VGA_HEIGHT; y++ )
	{
		for ( size_t x = 0; x < VGA_WIDTH; x++ )
		{
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
}
 
void terminal_setcolor(uint8_t color)
{
	terminal_color = color;
}
 
void terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

void terminal_update_cursor(size_t row, size_t col)
 {
    unsigned short position=(row*80) + col;
 
    // cursor LOW port to vga INDEX register
    outb(0x3D4, 0x0F);
    outb(0x3D5, (unsigned char)(position&0xFF));
    // cursor HIGH port to vga INDEX register
    outb(0x3D4, 0x0E);
    outb(0x3D5, (unsigned char )((position>>8)&0xFF));
 }

void terminal_putchar(char c)
{
	if(c=='\n'||c=='\r')
	{
		terminal_column=0;
		terminal_row++;
	}
	else
	{
		terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
		terminal_column++;
	}

	if (terminal_column == VGA_WIDTH  )
	{
 		terminal_row++;
		terminal_column = 0;
	}

	if (terminal_row == VGA_HEIGHT )
	{
		terminal_scroll();
	}
	terminal_update_cursor(terminal_row,terminal_column);	
	
}

void terminal_scroll(void)
{
	for(size_t i=0;i<VGA_HEIGHT-1;i++)
	{
		for(size_t j=0;j<VGA_WIDTH;j++)
		{
			const size_t new_index = i * VGA_WIDTH + j;
			const size_t old_index = (i+1) * VGA_WIDTH + j;
			terminal_buffer[new_index]=terminal_buffer[old_index];
		}
	}
	for(size_t j=0;j<VGA_WIDTH;j++)
	{
		terminal_putentryat(0, COLOR_WHITE, j, VGA_HEIGHT-1);
	}
	terminal_column = 0;
	terminal_row=VGA_HEIGHT-1;
	terminal_update_cursor(terminal_row,terminal_column);
}
 
void terminal_write(const char* data, size_t size)
{
	for ( size_t i = 0; i < size; i++ )
		terminal_putchar(data[i]);
}

void terminal_delete_front()
{
	
}

void terminal_delete_back()
{
	terminal_column--;
	terminal_putentryat(0, COLOR_WHITE, terminal_column, terminal_row);
	terminal_update_cursor(terminal_row, terminal_column);
	
}



 
void terminal_writestring(const char* data)
{
	terminal_write(data, strlen(data));
}
