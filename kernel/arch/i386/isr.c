//
// isr.c -- High level interrupt service routines and interrupt request handlers.
// Part of this code is modified from Bran's kernel development tutorials.
// Rewritten for JamesM's kernel development tutorials.
//

#include <common.h>
#include <kernel/isr.h>

#include <kernel/tty.h>
#include <drivers/portio.h>


isr_t interrupt_handlers[256];

void register_interrupt_handler(u8int n, isr_t handler)
{
  interrupt_handlers[n] = handler;
}


// This gets called from our ASM interrupt handler stub.
void isr_handler(registers_t* regs)
{
   puts("recieved interrupt\n");
   switch(regs->int_no)
	{
		case 0:
			puts("int 0\n");
			break;
		case 1:
			puts("int 1\n");
			break;
		case 2:
			puts("int 2\n");
			break;
		case 3:
			puts("int 3\n");
			break;
		case 4:
			puts("int 4\n");
			break;
		case 5:
			puts("int 5\n");
			break;
		case 6:
			puts("int 6\n");
			break;
		case 7:
			puts("int 7\n");
			break;
		case 8:
			puts("int 8\n");
			break;
		case 9:
			puts("int 9\n");
			break;
		case 10:
			puts("int 10\n");
			break;
		case 11:
			puts("int 11\n");
			break;
		case 12:
			puts("int 12\n");
			break;
		case 13:
			puts("int 13\n");
			break;
		case 14:
			puts("int 14\n");
			break;
		case 15:
			puts("int 15\n");
			break;
		case 16:
			puts("int 16\n");
			break;
		case 17:
			puts("int 17\n");
			break;
		case 18:
			puts("int 18\n");
			break;
		case 19:
			puts("int 19\n");
			break;
		case 20:
			puts("int 20\n");
			break;
		case 21:
			puts("int 21\n");
			break;
		case 22:
			puts("int 22\n");
			break;
		case 23:
			puts("int 23\n");
			break;
		case 24:
			puts("int 24\n");
			break;
		case 25:
			puts("int 25\n");
			break;
		case 26:
			puts("int 26\n");
			break;
		case 27:
			puts("int 27\n");
			break;
		case 28:
			puts("int 28\n");
			break;
		case 29:
			puts("int 29\n");
			break;
		case 30:
			puts("int 30\n");
			break;
		case 31:
			puts("int 31\n");
			break;

	}
}


// This gets called from our ASM interrupt handler stub.
void irq_handler(registers_t* regs)
{
   // Send an EOI (end of interrupt) signal to the PICs.
   // If this interrupt involved the slave.
   if (regs->int_no >= 40)
   {
       // Send reset signal to slave.
       outb(0xA0, 0x20);
   }
   // Send reset signal to master. (As well as slave, if necessary).
   outb(0x20, 0x20);

   if (interrupt_handlers[regs->int_no] != 0)
   {
       isr_t handler = interrupt_handlers[regs->int_no];
       handler(*regs);
   }
}





